FROM jlesage/baseimage-gui:debian-11

#########################################
##        ENVIRONMENTAL CONFIG         ##
#########################################

# User/Group Id gui app will be executed as default are 99 and 100
ENV USER_ID=99
ENV GROUP_ID=100

# Gui App Name default is "GUI_APPLICATION"
ENV APP_NAME="OpenRGB"

# Default resolution, change if you like
ENV DISPLAY_WIDTH=1280
ENV DISPLAY_HEIGHT=720

# Use a secure connection to the GUI
ENV SECURE_CONNECTION 1

# Generate and install favicons.
RUN \
    APP_ICON_URL=https://assets.gitlab-static.net/uploads/-/system/project/avatar/10582521/OpenRGB.png && \
    install_app_icon.sh "$APP_ICON_URL"

#########################################
##    REPOSITORIES AND DEPENDENCIES    ##
#########################################

WORKDIR /

RUN add-pkg \
	wget \
	libusb-1.0-0-dev \
	i2c-tools \
	qtbase5-dev \
	qtchooser \
	qt5-qmake \
	qtbase5-dev-tools \
	libhidapi-dev \
	libmbedcrypto3 \
	libmbedtls12 \
	libmbedx509-0
RUN add-pkg --virtual \
	build-dependencies \
	qtcreator \
	make \
	ca-certificates

RUN wget https://openrgb.org/releases/release_0.8/openrgb_0.8_amd64_bullseye_fb88964.deb

# This init script will run dpkg -i on the .deb file on each startup
# as well as run openrgb --server
COPY 04-openrgb.sh /etc/cont-init.d/

RUN chmod 777 /etc/cont-init.d/04-openrgb.sh

# Copy X app start script to correct location
COPY startapp.sh /startapp.sh

RUN chmod 777 /startapp.sh
